import sys
from Bio import SeqIO
fasta_file = "haplotype-GRA.fasta"
remove_file = "contaminated_read_ids.txt"
result_file = "haplotype-GRA_rmv_contam_reads.fasta"
with open(result_file, "w") as a, open(remove_file, "r") as b:
	reader = b.read().splitlines()
	for seq in SeqIO.parse(fasta_file, "fasta"):
		if seq.id not in reader:
			SeqIO.write(seq, a, "fasta")


import sys
from Bio import SeqIO
fasta_file = "haplotype-URO.fasta"
remove_file = "contaminated_read_ids_uro.txt"
result_file = "haplotype-URO_rmv_contam_reads.fasta"
with open(result_file, "w") as a, open(remove_file, "r") as b:
	reader = b.read().splitlines()
	for seq in SeqIO.parse(fasta_file, "fasta"):
		if seq.id not in reader:
			SeqIO.write(seq, a, "fasta")

