#!/bin/bash
#SBATCH --job-name=minimap2
#SBATCH -N 1
#SBATCH -n 1
#SBATCH -c 32
#SBATCH --partition=general
#SBATCH --qos=general
#SBATCH --mail-type=END
#SBATCH --mem=250G
#SBATCH --mail-user=anneri.lotter@fabi.up.ac.za
#SBATCH -o %x_%j.out
#SBATCH -e %x_%j.err

module load minimap2/2.17
## For short-read alignments
minimap2 -ax sr ./../AllChr_mt_ch.fasta /projects/EBP/CBC/eucalyptus/rawReads/Illumina/GRA_FK1758_unclassified_1.fastq.tar.gz /projects/EBP/CBC/eucalyptus/rawReads/Illumina/GRA_FK1758_unclassified_2.fastq.tar.gz > gravsassembled.sam
minimap2 -ax sr ./../AllChr_mt_ch.fasta /projects/EBP/CBC/eucalyptus/rawReads/Illumina/URO_FK1556_unclassified_1.fastq.tar.gz /projects/EBP/CBC/eucalyptus/rawReads/Illumina/URO_FK1556_unclassified_2.fastq.tar.gz > urovsassembled.sam


module load samtools/1.9
samtools view -S -b gravsassembled.sam > gravsassembled.bam
samtools sort gravsassembled.bam > gravsassembled.sorted.bam
samtools index gravsassembled.sorted.bam
samtools depth gravsassembled.sorted.bam > gravsassembled.txt
samtools stats -c 1,1000,1 gravsassembled.sorted.bam > gracov.txt

samtools view -S -b urovsassembled.sam > urovsassembled.bam
samtools sort urovsassembled.bam > urovsassembled.sorted.bam
samtools index urovsassembled.sorted.bam
samtools depth urovsassembled.sorted.bam > urovsassembled.txt
samtools stats -c 1,1000,1 urovsassembled.sorted.bam > urocov.txt

## For long-read bins
minimap2 -ax map-ont ./../AllChr_mt_ch.fasta haplotype-GRA_rmv_contam_reads.fasta > grahapvsv2.sam
samtools view -S -b grahapvsv2.sam > grahapvsv2.bam
samtools sort grahapvsv2.bam > grahapvsv2.sorted.bam
samtools index grahapvsv2.sorted.bam
samtools depth grahapvsv2.sorted.bam > grahapvsv2.txt
samtools stats -c 1,1000,1 grahapvsv2.sorted.bam > grahapvsv2.txt

minimap2 -ax map-ont ./../AllChr_mt_ch.fasta haplotype-URO_rmv_contam_reads.fasta > urohapvsv2.sam
samtools view -S -b urohapvsv2.sam > urohapvsv2.bam
samtools sort urohapvsv2.bam > urohapvsv2.sorted.bam
samtools index urohapvsv2.sorted.bam
samtools depth urohapvsv2.sorted.bam > urohapvsv2.txt
samtools stats -c 1,1000,1 urohapvsv2.sorted.bam > urohapvsv2.txt


## For assemblies
minimap2 -ax asm20 ./../AllChr_mt_ch.fasta gra_hap_unmasked.fasta > gravsv2.sam
samtools view -S -b gravsv2.sam > gravsv2.bam
samtools sort gravsv2.bam > gravsv2.sorted.bam
samtools index gravsv2.sorted.bam
samtools depth gravsv2.sorted.bam > gravsv2.txt
samtools stats -c 1,1000,1 gravsv2.sorted.bam > gravsv2.txt

minimap2 -ax asm20 ./../AllChr_mt_ch.fasta uro_hap_unmasked.fasta > urovsv2.sam
samtools view -S -b urovsv2.sam > urovsv2.bam
samtools sort urovsv2.bam > urovsv2.sorted.bam
samtools index urovsv2.sorted.bam
samtools depth urovsv2.sorted.bam > urovsv2.txt
samtools stats -c 1,1000,1 urovsv2.sorted.bam > urovsv2.txt

## Create bedgraphs that can be viewed in IGV
module load deeptools/2.0
bamCoverage -b gravsassembled.sorted.bam -of bedgraph -o grandis_100Kb.bg --normalizeUsingRPKM -bs 100000
bamCoverage -b urovsassembled.sorted.bam -of bedgraph -o urophylla_100Kb.bg -bs 100000 --normalizeUsingRPKM
bamCoverage -b urohapvsv2.sorted.bam -of bedgraph -o urohapvsv2_100Kb.bg -bs 100000 --normalizeUsingRPKM
bamCoverage -b grahapvsv2.sorted.bam -of bedgraph -o grahapvsv2_100Kb.bg -bs 100000 --normalizeUsingRPKM
bamCoverage -b urovsv2.sorted.bam -of bedgraph -o urovsv2_100Kb.bg -bs 100000 --normalizeUsingRPKM
bamCoverage -b gravsv2.sorted.bam -of bedgraph -o gravsv2_100Kb.bg -bs 100000 --normalizeUsingRPKM


## After identifying (visually) bins with high coverage, the depth data is extracted using awk for the bins per alignment

awk '{ if(($1=="Chr01") && ($2>=27900000 && $2<=28000000)) print $0}' gravsassembled.txt > gravsassembledfilt.txt
awk '{ if(($1=="Chr02") && ($2>=22300000 && $2<=22500000)) print $0}' gravsassembled.txt >> gravsassembledfilt.txt
awk '{ if(($1=="Chr06") && ($2>=27800000 && $2<=27900000)) print $0}' gravsassembled.txt >> gravsassembledfilt.txt
awk '{ if(($1=="Chr09") && ($2>=28300000 && $2<=28500000)) print $0}' gravsassembled.txt >> gravsassembledfilt.txt
awk '{ if(($1=="Chr10") && ($2>=0 && $2<=100000)) print $0}' gravsassembled.txt >> gravsassembledfilt.txt
awk '{ if(($1=="Chr10") && ($2>=32700000 && $2<=32800000)) print $0}' gravsassembled.txt >> gravsassembledfilt.txt
awk '{ if(($1=="Chr11") && ($2>=24000000 && $2<=24100000)) print $0}' gravsassembled.txt >> gravsassembledfilt.txt


awk '{ if(($1=="Chr02") && ($2>=22300000 && $2<=22500000)) print $0}' urovsassembled.txt > urovsassembledfilt.txt
awk '{ if(($1=="Chr06") && ($2>=27800000 && $2<=27900000)) print $0}' urovsassembled.txt >> urovsassembledfilt.txt
awk '{ if(($1=="Chr09") && ($2>=28300000 && $2<=28500000)) print $0}' urovsassembled.txt >> urovsassembledfilt.txt
awk '{ if(($1=="Chr10") && ($2>=0 && $2<=100000)) print $0}' urovsassembled.txt >> urovsassembledfilt.txt
awk '{ if(($1=="Chr11") && ($2>=24000000 && $2<=24100000)) print $0}' urovsassembled.txt >> urovsassembledfilt.txt


awk '{ if(($1=="Chr02") && ($2>=22300000 && $2<=22500000)) print $0}' grahapvsv2.txt > grahapvsv2filt.txt
awk '{ if(($1=="Chr06") && ($2>=20000000 && $2<=20100000)) print $0}' grahapvsv2.txt >> grahapvsv2filt.txt
awk '{ if(($1=="Chr06") && ($2>=27800000 && $2<=27900000)) print $0}' grahapvsv2.txt >> grahapvsv2filt.txt
awk '{ if(($1=="Chr07") && ($2>=28700000 && $2<=28800000)) print $0}' grahapvsv2.txt >> grahapvsv2filt.txt
awk '{ if(($1=="Chr10") && ($2>=0 && $2<=100000)) print $0}' grahapvsv2.txt >> grahapvsv2filt.txt

awk '{ if(($1=="Chr02") && ($2>=22300000 && $2<=22500000)) print $0}' urohapvsv2.txt > urohapvsv2filt.txt
awk '{ if(($1=="Chr06") && ($2>=27800000 && $2<=27900000)) print $0}' urohapvsv2.txt >> urohapvsv2filt.txt
awk '{ if(($1=="Chr08") && ($2>=0 && $2<=100000)) print $0}' urohapvsv2.txt >> urohapvsv2filt.txt
awk '{ if(($1=="Chr09") && ($2>=28300000 && $2<=28500000)) print $0}' urohapvsv2.txt >> urohapvsv2filt.txt
awk '{ if(($1=="Chr10") && ($2>=0 && $2<=100000)) print $0}' urohapvsv2.txt >> urohapvsv2filt.txt

awk '{ if(($1=="Chr01") && ($2>=27900000 && $2<=28000000)) print $0}' gravsv2.txt > gravsv2filt.txt
awk '{ if(($1=="Chr02") && ($2>=22300000 && $2<=22500000)) print $0}' gravsv2.txt >> gravsv2filt.txt
awk '{ if(($1=="Chr03") && ($2>=500000 && $2<=600000)) print $0}' gravsv2.txt >> gravsv2filt.txt
awk '{ if(($1=="Chr04") && ($2>=0 && $2<=100000)) print $0}' gravsv2.txt >> gravsv2filt.txt
awk '{ if(($1=="Chr06") && ($2>=20000000 && $2<=20100000)) print $0}' gravsv2.txt >> gravsv2filt.txt
awk '{ if(($1=="Chr07") && ($2>=28700000 && $2<=28800000)) print $0}' gravsv2.txt >> gravsv2filt.txt
awk '{ if(($1=="Chr08") && ($2>=0 && $2<=100000)) print $0}' gravsv2.txt >> gravsv2filt.txt
awk '{ if(($1=="Chr10") && ($2>=0 && $2<=100000)) print $0}' gravsv2.txt >> gravsv2filt.txt

awk '{ if(($1=="Chr02") && ($2>=22300000 && $2<=22500000)) print $0}' urovsv2.txt > urovsv2filt.txt
awk '{ if(($1=="Chr07") && ($2>=4200000 && $2<=4300000)) print $0}' urovsv2.txt >> urovsv2filt.txt
awk '{ if(($1=="Chr08") && ($2>=0 && $2<=100000)) print $0}' urovsv2.txt >> urovsv2filt.txt
awk '{ if(($1=="Chr10") && ($2>=0 && $2<=100000)) print $0}' urovsv2.txt >> urovsv2filt.txt


## After isolating bins, find the average in each alignment and isolate everything with greater than average depth per file using awk
awk '{sum+=$3} END { print "Average = ",sum/NR}' grahapvsv2filt.txt
awk '{sum+=$3} END { print "Average = ",sum/NR}' gravsassembledfilt.txt
awk '{sum+=$3} END { print "Average = ",sum/NR}' urovsassembledfilt.txt
awk '{sum+=$3} END { print "Average = ",sum/NR}' urohapvsv2filt.txt
awk '{sum+=$3} END { print "Average = ",sum/NR}' gravsv2filt.txt
awk '{sum+=$3} END { print "Average = ",sum/NR}' urovsv2filt.txt

## Using average, get everything above average into file
awk '{if($3>512) print$0 }' urovsassembledfilt.txt > urovsassembledfilt1.txt
awk '{if($3>599) print$0 }' gravsassembledfilt.txt > gravsassembledfilt1.txt
awk '{if($3>885) print$0 }' grahapvsv2filt.txt > grahapvsv2filt1.txt
awk '{if($3>652) print$0 }' urohapvsv2filt.txt > urohapvsv2filt1.txt
awk '{if($3>6.7) print$0 }' gravsv2filt.txt > gravsv2filt1.txt
awk '{if($3>5.8) print$0 }' urovsv2filt.txt > urovsv2filt1.txt

## Filter for inflection points within the bins to get the start and stops for the fasta sequence to be extracted (python script)
## Cat all bins into a single file

## Extract fasta sequence from E. grandis v2.0 reference genome

module load BEDtools/2.29.0
bedtools getfasta -fullHeader -fi AllChr_mt_ch.fasta -bed gra_bins.txt -fo gra_bins.fasta
bedtools getfasta -fullHeader -fi AllChr_mt_ch.fasta -bed uro_bins.txt -fo uro_bins.fasta

## Once we have the fasta sequences, generate blast databases for the chloroplast and mitochondrial genomes
module load blast/2.7.1
makeblastdb -in gra_bins.fasta -dbtype nucl -out gra_bin_db
makeblastdb -in uro_bins.fasta -dbtype nucl -out uro_bin_db

## And blast sequences to databases
blastn -num_threads 32 -query gra_bins.fasta -db chloroplast_db -evalue 0.00001 -outfmt 6 -num_alignments 1 -max_hsps 1 -out gra_chl.blastp
blastn -num_threads 32 -query gra_bins.fasta -db mitochondrial_db -evalue 0.00001 -outfmt 6 -num_alignments 1 -max_hsps 1 -out gra_mt.blastp

blastn -num_threads 32 -query uro_bins.fasta -db chloroplast_db -evalue 0.00001 -outfmt 6 -num_alignments 1 -max_hsps 1 -out uro_chl.blastp
blastn -num_threads 32 -query uro_bins.fasta -db mitochondrial_db -evalue 0.00001 -outfmt 6 -num_alignments 1 -max_hsps 1 -out uro_mt.blastp

## For extracted sequences make repeat libraries
module load RepeatModeler/1.0.8
module load rmblastn/2.2.28
module load perl/5.24.0
export PERL5LIB=/labs/Wegrzyn/perl5/lib/perl5/
export PERLINC=/labs/Wegrzyn/perl5/lib/perl5/

#BuildDatabase -name "gra" gra_bins.fasta 2>&1 | tee buildDB.log
#RepeatModeler -engine ncbi -pa 22 -database "gra" 2>&1 | tee repeatModelergra.log

#BuildDatabase -name "uro" uro_bins.fasta 2>&1 | tee buildDB.log
#RepeatModeler -engine ncbi -pa 22 -database "uro" 2>&1 | tee repeatModeleruro.log

## And get repeat alignments
module load RepeatMasker/4.0.9-p2
RepeatMasker gra_bins.fasta -dir rpm/grandis -lib RM_117506.ThuMay130207192021/consensi.fa.classified -dir rpm/gra -pa 4 -gff -a -noisy -nolow -xsmall 1>RepeatMaskerrpm2_1.log 2>RepeatMaskerrpm_1.err
RepeatMasker uro_bins.fasta -dir rpm/uro -lib RM_127803.ThuMay130211242021/consensi.fa.classified -dir rpm/uro -pa 4 -gff -a -noisy -nolow -xsmall 1>RepeatMaskerrpm2_2.log 2>RepeatMaskerrpm_2.err


