#!/bin/bash
#SBATCH --job-name=minimap2
#SBATCH -N 1
#SBATCH -n 1
#SBATCH -c 32
#SBATCH --partition=general
#SBATCH --qos=general
#SBATCH --mail-type=END
#SBATCH --mem=250G
#SBATCH --mail-user=anneri.lotter@fabi.up.ac.za
#SBATCH -o %x_%j.out
#SBATCH -e %x_%j.err

## For short-read alignments
module load minimap2/2.17
minimap2 -ax sr path/to/genome path/to/SR_1.fq path/to/SR_2.fq > out.sam

## For long-read bins
minimap2 -ax map-ont path/to/genome path/to/LR_binned.fa > out.sam

## For assemblies
minimap2 -ax asm20 path/to/genome path/to/assembly > out.sam

## Convert to bam and get coverage
module load samtools/1.9
samtools view -S -b out.sam > out.bam
samtools sort out.bam > out.sorted.bam
samtools index out.sorted.bam
samtools depth out.sorted.bam > out.txt

## Create bedgraphs that can be viewed in IGV
module load deeptools/2.0
bamCoverage -b out.sorted.bam -of bedgraph -o out_100K.bg --normalizeUsingRPKM -bs 100000

## After identifying (visually) bins with high coverage, the depth data is extracted using awk for the bins per alignment
awk '{ if(($1=="Chr") && ($2>=binstart && $2<=binend)) print $0}' out.txt > outfilt.txt

## After isolating bins, find the average in each alignment and isolate everything with greater than average depth per file using awk
awk '{sum+=$3} END { print "Average = ",sum/NR}' outfilt.txt


