## [**Supplementary Note 1: Hap-mer based phasing completeness assessment**](https://gitlab.com/Anneri/eucalyptus-haplogenome-synteny/-/tree/main/9%20Supplementary%20notes/1-Hapmer%20separation%20and%20binning%20assessment)

To evaluate the size of phased block and phase correctness of our haplo_genome assemblies, we used [Merqury v1.0](https://github.com/marbl/merqury). Merqury basically check whether the haplotype specific k-mers found in the short-read dataset before binning corresponds to those found in the assembly, and whether the correct parental k-mers are present in the assembly.

**Input:**
* Subset of the meryl database created by **Canu** (the parent specific mers used for haplotype binning, please see [merqury website](https://github.com/marbl/merqury/wiki/1.-Prepare-meryl-dbs) for more details)
* [merqury.sh](9 Supplementary notes/1-Hapmer separation and binning assessment/merqury.sh) script to create child meryl database and run merqury

**Output:**

* A bunch of files are generated, but we are interested in:
    *   ***.hapmers.count** file used to generate blob plots and the blob plots
    *   ***.100_20000.phased_block.sizes** and ***.10_20000.phased_block.sizes** used to generate phase block stats at 100 and 10 (strict) switch errors per 20 kb block
    *   ***.contig/scaffold/phased_block.sizes** to check phase block sizes compared to contig/scaffold sizes

## **Supplementary Note 2:  Read and assembly alignment and validation of high peak content**

First we aligned short-read and long-read binned sequencing data to the _E. grandis_ v2.0 reference genome with **minimap**. We then convert the resulting SAM file to BAM with **SAMtools** and get coverage in 100 kb bins with **deeptools**. The resulting bed files were visualised in IGV browser and high coverage bins identified visually. 


Within the high coverage bins we looked for the boundaries (where depth increased and decreased) were used to determine the position of the fasta sequence where the genome sequence was extracted using getFasta from **BEDtools**. 

We also created a blast database for the mitochondrial and chloroplast genomes, against which the extracted fasta sequences were blasted.

Lastly to look at repeat content, we used RepeatModeler and Repeatmasker to find repeats in the extracted fasta fiels. 
