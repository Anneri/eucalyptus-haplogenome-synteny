#!/bin/bash
#PBS -l nodes=1:ppn=28
#PBS -l walltime=30:00:00
#PBS -q normal
#PBS -o /nlustre/users/anneri/repeatmasking/gra_ltr
#PBS -e /nlustre/users/anneri/repeatmasking/gra_ltr
#PBS -k oe
#PBS -m abe
#PBS -M anneri.lotter@fabi.up.ac.za

module load ltr_finder-1.0.6

cd /nlustre/users/anneri/repeatmasking/gra_ltr
perl -nle 's/scf718000000//g; print $_' gra_PS_split_AM.fasta > gra_PS_split_AM.modified.fasta

#ltr_finder
ltr_finder -D 15000 -d 1000 -L 7000 -l 100 -p 20 -C -M 0.9 gra_PS_split_AM.modified.fasta > gra_PS_split_AM.finder.scn
#ltr_finder -D 15000 -d 1000 -L 7000 -l 100 -p 20 -C -M 0.9 gra_PS_split_AM.fasta -w 1 2 > LTRfinder.out

module load genometools-1.5.9
#ltrharvest
gt suffixerator -db gra_PS_split_AM.modified.fasta -indexname gra_PS_split_AM.modified.fasta -tis -suf -lcp -des -ssp -sds -dna

gt ltrharvest -index gra_PS_split_AM.modified.fasta -similar 90 -vic 10 -seed 20 -seqids yes -minlenltr 100 -maxlenltr 7000 -mintsd 4 -maxtsd 6 -motif TGCA -motifmis 1 > gra_PS_split_AM.harvest.scn
gt ltrharvest -index gra_PS_split_AM.modified.fasta -similar 90 -vic 10 -seed 20 -seqids yes -minlenltr 100 -maxlenltr 7000 -mintsd 4 -maxtsd 6  > gra_PS_split_AM.harvest.nonTGCA.scn

module load maker-2.31.10
module load ltr_retriever
module load cdhit
module load hmmer-3.1b2

#run LTR_retriever to get the LAI score  
#LTR_retriever -genome gra_PS_split_AM.fasta -inharvest gra_PS_split_AM.harvest.scn -infinder gra_PS_split_AM.finder.scn -nonTGCA gra_PS_split_AM.harvest.nonTGCA.scn -threads 20

#LTR_retriever -genome gra_PS_split_AM.fasta -inharvest gra_PS_split_AM.harvest.scn -infinder gra_PS_split_AM.finder.scn -nonTGCA gra_PS_split_AM.harvest.nonTGCA.scn -threads 20 -cdhit_path /apps/cdhit/ -hmmer /apps/hmmer-3.1b2/bin

#perl -nle 's/scf718000000//g; print $_' gra_PS_split_AM.finder.scn > gra_PS_split_AM.modified.finder.scn
LTR_retriever -genome gra_PS_split_AM.modified.fasta -inharvest gra_PS_split_AM.harvest.scn -infinder gra_PS_split_AM.finder.scn -nonTGCA gra_PS_split_AM.harvest.nonTGCA.scn -threads 20
