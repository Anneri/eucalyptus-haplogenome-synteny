#!/bin/bash
#SBATCH --job-name=rpm
#SBATCH -N 1
#SBATCH -n 1
#SBATCH -c 32
#SBATCH --partition=general
#SBATCH --qos=general
#SBATCH --mail-type=END
#SBATCH --mem=250G
#SBATCH --mail-user=anneri.lotter@fabi.up.ac.za
#SBATCH -o repeatmasker-%j.output 
#SBATCH -e repeatmasker-%j.error 

# Run the program                 
echo GU2.fasta

#module load RepeatMasker/4.1.1 
#module load perl/5.24.0
#export PERL5LIB=/labs/Wegrzyn/perl5/lib/perl5/
#export PERLINC=/labs/Wegrzyn/perl5/lib/perl5/

module load RepeatMasker/4.0.9-p2

reference='GU2.fasta'
lib='uro_all_lib1.fa' #created by RepeatModeler (consensi.fa.classified) and LTR_retriever (*.LTRlib.fa)
outputDir="02_repeatMasker_rpm2/"
genomeSize=650M
#use 4 per one threads, if set it to 10, it will use 40
threads=10

divPath=$outputDir/$GENOME_NAME.repeats.divsum

if [ ! -d $outputDir ]; then
    mkdir $outputDir
fi

rm $outputDir/*

ln -s $reference $outputDir

echo 'begin to run repeatMasker'

RepeatMasker GU2.fasta -dir 02_repeatMasker_rpm2/ -lib ./RM_39354.MonMay250651022020/uro_all_lib1.fa $outputDir/$(basename $reference) -pa 4 -gff -a -noisy -nolow -xsmall 1>RepeatMaskerrpm2_1.log 2>RepeatMaskerrpm2_1.err

#/isg/shared/apps/RepeatMasker/4.0.9-p2/util/calcDivergenceFromAlign.pl -s $divPath $outputDir/$(basename $reference).align

#/isg/shared/apps/RepeatMasker/4.0.9-p2/util/createRepeatLandscape.pl -div $divPath -g $genomeSize > $outputDir/$GENOME_NAME.repeats.html

#/isg/shared/apps/RepeatMasker/4.0.9-p2/util/buildSummary.pl -species urophylla -genome *.tsv


# The pipe "|" here creates an output text file that can be checked for errors
#RepeatMasker GU2.fasta -dir /scratch/alotter/repeatmodeler/urophylla/RM_39354.MonMay250651022020/repeatmasker_out -lib /scratch/alotter/repeatmodeler/urophylla/RM_39354.MonMay250651022020/consensi.fa.classified -pa 4 -gff -a -noisy -low -xsmall| tee GU2.txt

#RepeatMasker uro.PS_AM_unmasked.modified.fasta -dir /result_uro/RM_ltr -lib /result_uro/uro.PS_AM_unmasked.modified.fasta.mod.LTRlib.fa -pa 4 -gff -a -noisy -low -xsmall| tee ltr_uro.txt
