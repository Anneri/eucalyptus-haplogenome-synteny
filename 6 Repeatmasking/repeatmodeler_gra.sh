#!/bin/bash
#SBATCH --job-name=rpmodel
#SBATCH -N 1
#SBATCH -n 1
#SBATCH -c 32
#SBATCH --partition=general
#SBATCH --qos=general
#SBATCH --mail-type=ALL
#SBATCH --mem=250G
#SBATCH --mail-user=anneri.lotter@fabi.up.ac.za
#SBATCH -o repeatmodeler-%j.output
#SBATCH -e repeatmodeler-%j.error

# Run the program
export DATADIR='../'
export SEQFILE="gra_PS.fasta"
export DATABASE="grandis"

#mkdir repeatmodeler/gra_split
WORKDIR=repeatmodeler/gra_split/$JOB_ID 
#mkdir -p $WORKDIR 
cp $DATADIR/$SEQFILE $WORKDIR 
cd $WORKDIR

# Note that modules listed here may not be up to date or present when you are trying to run this script
module load RepeatModeler/1.0.8 
module load rmblastn/2.2.28 
module load perl/5.24.0 
export PERL5LIB=/labs/Wegrzyn/perl5/lib/perl5/ 
export PERLINC=/labs/Wegrzyn/perl5/lib/perl5/

#module load perl/5.28.1

BuildDatabase -name "grandis" gra_PS.fasta 2>&1 | tee buildDB.log
RepeatModeler -engine ncbi -pa 22 -database $DATABASE 2>&1 | tee repeatModeler.log

