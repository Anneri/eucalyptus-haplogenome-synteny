#!/bin/bash
#SBATCH --job-name=syri
#SBATCH -n 1
#SBATCH -N 1
#SBATCH -c 16
#SBATCH --mem=350G
#SBATCH --partition=himem2
#SBATCH --qos=himem
#SBATCH --mail-type=END
#SBATCH --mail-user=anneri.lotter@fabi.up.ac.za
#SBATCH -o %x_%A.out
#SBATCH -e %x_%A.err

module load samtools/1.3.1
module load minimap2/2.17
minimap2 -ax asm5 --eqx ../../uro.PS_AM_unmasked.chr.fasta ../../sorted_chr.fa \
| samtools sort -O BAM - > euhap_egv2.bam



module load MUMmer/3.23

# Using SyRI to identify genomic rearrangements from whole-genome alignments generated using MUMmer. A .tsv (out.filtered.coords) file is used as the input.
# Whole genome alignment. Any other alignment can also be used.
nucmer --maxmatch -c 100 -b 500 -l 50 ../../uro.PS_AM_unmasked.chr.fasta ../../sorted_chr.fa -p euhap_egv2

# Remove small and lower quality alignments
delta-filter -m -i 90 -l 100 euhap_egv2.delta > euhap_egv2.filtered.delta
# Convert alignment information to a .TSV format as required by SyRI
show-coords -THrd euhap_egv2.filtered.delta > euhap_egv2.filtered.coords

nucmer --maxmatch -l 100 -c 500 ../../uro.PS_AM_unmasked.chr.fasta ../../sorted_chr.fa -prefix asb_euhap_egv2

. ~/miniconda3/etc/profile.d/conda.sh
conda activate ~/miniconda3/envs/py35/
cwd="."     # Change to working directory
PATH_TO_SYRI="../../syri/bin/syri" #Change the path to point to syri executable
PATH_TO_PLOTSR="../../syri/bin/plotsr" #Change the path to point to plotsr executable


python3 $PATH_TO_SYRI -c euhap_egv2.bam -r ../../uro.PS_AM_unmasked.chr.fasta -q ../../sorted_chr.fa -k -F B --no-chrmatch --prefix euhap_egv2
python3 $PATH_TO_SYRI -c euhap_egv2.filtered.coords -d euhap_egv2.filtered.delta -r ../../uro.PS_AM_unmasked.chr.fasta -q ../../sorted_chr.fa -k --no-chrmatch --prefix euhap_egv2

# Plot genomic structure
python3 $PATH_TO_PLOTSR euhap_egv2syri.out -r ../../uro.PS_AM_unmasked.chr.fasta -q ../../sorted_chr.fa -o pdf -d 600

