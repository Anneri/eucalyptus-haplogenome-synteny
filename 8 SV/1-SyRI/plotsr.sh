#!/bin/bash
#SBATCH --job-name=plotsr
#SBATCH -n 1
#SBATCH -N 1
#SBATCH -c 16
#SBATCH --mem=150G
#SBATCH --partition=himem
#SBATCH --qos=himem
#SBATCH --mail-type=END
#SBATCH --mail-user=anneri.lotter@fabi.up.ac.za
#SBATCH -o %x_%A.out
#SBATCH -e %x_%A.err

. ~/miniconda3/etc/profile.d/conda.sh
conda activate ~/miniconda3/envs/py38
#conda activate ~/miniconda3/envs/py35

plotsr \
	--sr v2_eghapsyri.out \
	--sr eghap_euhapsyri.out \
	--sr euhap_egv2syri.out \
	--chrord chrord.txt \
	--genomes genomes.txt \
	-o egv2_eg_eu_egv2.svg \
	--cfg base.cfg \
	-s 10000 -f 8\
	--log DEBUG

plotsr \
	--sr v2_eghapsyri.out \
	--sr eghap_euhapsyri.out \
	--sr euhap_egv2syri.out \
	--chr Chr01 --chr Chr02 --chr Chr03 --chr Chr04 --chr Chr05 --chr Chr06 \
	--genomes genomes.txt \
	-o egv2_eg_eu_egv2.chr1_6.svg \
	--cfg base.cfg \
	-s 10000 -f 8 \
	--log DEBUG

plotsr \
	--sr v2_eghapsyri.out \
	--sr eghap_euhapsyri.out \
	--sr euhap_egv2syri.out \
	--chr Chr07 --chr Chr08 --chr Chr09 --chr Chr10 --chr Chr11 \
	--genomes genomes.txt \
	-o egv2_eg_eu_egv2.chr7_11.svg \
	--cfg base.cfg \
	-s 10000 -f 8 \
	--log DEBUG

plotsr \
	--sr v2_eghapsyri.out \
	--sr eghap_euhapsyri.out \
	--sr euhap_egv2syri.out \
	--chr Chr01 --chr Chr02 --chr Chr03 --chr Chr04 \
	--genomes genomes.txt \
	-o egv2_eg_eu_egv2.chr1_4.svg \
	--cfg base.cfg \
	-s 10000 -f 8 \
	--log DEBUG

plotsr \
	--sr v2_eghapsyri.out \
	--sr eghap_euhapsyri.out \
	--sr euhap_egv2syri.out \
	--chr Chr05 --chr Chr06 --chr Chr07 --chr Chr08 \
	--genomes genomes.txt \
	-o egv2_eg_eu_egv2.chr5_8.svg \
	--cfg base.cfg \
	-s 10000 -f 8 \
	--log DEBUG

plotsr \
	--sr v2_eghapsyri.out \
	--sr eghap_euhapsyri.out \
	--sr euhap_egv2syri.out \
	--chr Chr09 --chr Chr10 --chr Chr11 \
	--genomes genomes.txt \
	-o egv2_eg_eu_egv2.chr9_11.svg \
	--cfg base.cfg \
	-s 10000 -f 8 \
	--log DEBUG

