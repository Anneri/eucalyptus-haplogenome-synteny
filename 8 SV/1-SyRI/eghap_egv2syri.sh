#!/bin/bash
#SBATCH --job-name=syri
#SBATCH -n 1
#SBATCH -N 1
#SBATCH -c 32
#SBATCH --mem=250G
#SBATCH --partition=general
#SBATCH --qos=general
#SBATCH --mail-type=END
#SBATCH --mail-user=anneri.lotter@fabi.up.ac.za
#SBATCH -o %x_%A.out
#SBATCH -e %x_%A.err

module load samtools/1.3.1
module load minimap2/2.17
#minimap2 -ax asm5 --eqx ../../gra_PS_split_AM.chr.fasta ../../uro.PS_AM_unmasked.chr.fasta > grarescafvsurohap.sam
minimap2 -ax asm5 --eqx ../../sorted_chr.fa ../../gra_PS_split_AM.chr.fasta > grahapvsv2.0.sam


#samtools view -b grarescafvsurohap.sam > grarescafvsurohap.bam
samtools view -b grahapvsv2.0.sam > grahapvsv2.0.bam

module load MUMmer/3.23

# Using SyRI to identify genomic rearrangements from whole-genome alignments generated using MUMmer. A .tsv (out.filtered.coords) file is used as the input.
# Whole genome alignment. Any other alignment can also be used.
nucmer --maxmatch -c 100 -b 500 -l 50 ../../sorted_chr.fa ../../gra_PS_split_AM.chr.fasta -p grahapvsv2.0

# Remove small and lower quality alignments
delta-filter -m -i 90 -l 100 grahapvsv2.0.delta > grahapvsv2.0.filtered.delta
# Convert alignment information to a .TSV format as required by SyRI
show-coords -THrd grahapvsv2.0.filtered.delta > grahapvsv2.0.filtered.coords

show-coords -THrd grahapvsv2.0.delta > grahapvsv2.0.coords

nucmer --maxmatch -l 100 -c 500 ../../sorted_chr.fa ../../gra_PS_split_AM.chr.fasta -prefix asb_grahapvsv2.0

. ~/miniconda3/etc/profile.d/conda.sh
conda activate ~/miniconda3/envs/py35/
cwd="."     # Change to working directory
PATH_TO_SYRI="../../syri/bin/syri" #Change the path to point to syri executable
PATH_TO_PLOTSR="../../syri/bin/plotsr" #Change the path to point to plotsr executable


python3 $PATH_TO_SYRI -c grahapvsv2.0.sam ../../sorted_chr.fa ../../gra_PS_split_AM.chr.fasta -k -F B --no-chrmatch
python3 $PATH_TO_SYRI -c grahapvsv2.0.filtered.coords -d grahapvsv2.0.filtered.delta -r ../../sorted_chr.fa -q ../../gra_PS_split_AM.chr.fasta -k --no-chrmatch

# Plot genomic structure
python3 $PATH_TO_PLOTSR syri.out ../../sorted_chr.fa ../../gra_PS_split_AM.chr.fasta -o pdf -d 600

