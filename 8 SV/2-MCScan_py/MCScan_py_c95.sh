#!/bin/bash
#SBATCH --job-name=MCScan
#SBATCH -N 1
#SBATCH -n 1
#SBATCH -c 16
#SBATCH --partition=general
#SBATCH --qos=general
#SBATCH --mail-type=END
#SBATCH --mem=100G
#SBATCH --mail-user=anneri.lotter@fabi.up.ac.za
#SBATCH -o %x_%j.out
#SBATCH -e %x_%j.err


#srun --partition=general --qos=general --mem=150G --pty bash

. ~/miniconda3/etc/profile.d/conda.sh
conda activate last_jcvi


#Convert the GFF to BED file and rename them.
python -m jcvi.formats.gff bed --type=mRNA egr_OnlyChr.gff3 -o grandis.bed
python -m jcvi.formats.gff bed --type=mRNA eur_OnlyChr.gff3 -o urophylla.bed


#Reformat phytozome fasta
python -m jcvi.formats.fasta format eg.protein.faa grandis.cds
python -m jcvi.formats.fasta format eu.protein.faa urophylla.cds


#Make grandis primary for comparison
python -m jcvi.formats.gff bed --type=mRNA --key=Name --primary_only egr_OnlyChr.gff3 -o grandis.bed
python -m jcvi.formats.gff bed --type=mRNA --key=Name --primary_only eur_OnlyChr.gff3 -o urophylla.bed


#Do last comparison
#lastdb urophylla urophylla.cds
#lastal -u 0 -i3G -f BlastTab urophylla grandis.cds > ./grandis.urophylla.last
#python -m jcvi.compara.catalog ortholog grandis urophylla --no_strip_names

module load texlive
python -m jcvi.compara.catalog ortholog grandis urophylla --cscore=.95 --no_strip_names --genomenames="*Eucalyptus grandis*_*Eucalyptus urophylla*"

#Pairwise synteny dotplot
python -m jcvi.graphics.dotplot grandis.urophylla.anchors --genomenames="*Eucalyptus grandis*_*Eucalyptus urophylla*" --nmax=23470 -o grandis.urophylla.dotplot.pdf --dpi=300
python -m jcvi.graphics.dotplot grandis.urophylla.anchors --genomenames="*Eucalyptus grandis*_*Eucalyptus urophylla*" --nmax=23470 -o grandis.urophylla.dotplot.pdf --dpi=300
python -m jcvi.graphics.dotplot grandis.urophylla.anchors

#Synteny pattern check
python -m jcvi.compara.synteny depth --histogram grandis.urophylla.anchors --depthfile=grandis.urophylla.depth

#Macrosynteny visualization
python -m jcvi.compara.synteny screen --minspan=30 --simple grandis.urophylla.anchors grandis.urophylla.anchors.new
python -m jcvi.graphics.karyotype seqids layout --dpi=300 --format=pdf
python -m jcvi.graphics.karyotype seqids layout --dpi=300 --format=svg

#Microsynteny
python -m jcvi.compara.synteny mcscan grandis.bed grandis.urophylla.lifted.anchors --iter=1 -o grandis.urophylla.i1.blocks
python -m jcvi.compara.synteny mcscan grandis.bed grandis.urophylla.lifted.anchors -o grandis.urophylla.i100.blocks

#Make plot of synteny
cat grandis.bed urophylla.bed > grandis_urophylla.bed
python -m jcvi.graphics.synteny blocks grandis_urophylla.bed blocks.layout --glyphcolor=orientation --glyphstyle=arrow

head -50 grandis.urophylla.i1.blocks > blocks
python -m jcvi.graphics.synteny blocks grandis_urophylla.bed blocks.layout --glyphcolor=orientation --glyphstyle=arrow

head -50 grandis.urophylla.i100.blocks > blocks100
python -m jcvi.graphics.synteny blocks100 grandis_urophylla.bed blocks.layout --glyphcolor=orientation --glyphstyle=arrow
