library(readxl)
library(ggplot2)
library(cowplot)
library(dplyr)
library(tidyr)
library(imputeTS)
library(ggThemeAssist)
library(grid)
library(gridExtra)
library(ggpubr)


options(scipen=999)


##Specify units to use on axis in thousands, millions or billions to avoid zeros
addUnits <- function(n) {
  labels <- ifelse(n < 1000, n,  # less than thousands
                   ifelse(n < 1e6, paste0(round(n/1e3), 'k'),  # in thousands
                          ifelse(n < 1e9, paste0(round(n/1e6)),  # in millions I removed 'M' for this plot, but can be added as suffix if needed like for thousands
                                 ifelse(n < 1e12, paste0(round(n/1e9), 'G'), # in billions
                                        ifelse(n < 1e15, paste0(round(n/1e12), 'T'), # in trillions
                                               'too big!'
                                        )))))
  return(labels)
}


genomeSize <- read_xlsx("G:/My Drive/2019 MSc - Anneri Lotter/PhD/GenomeScope/all1.xlsx")

GS <- as.data.frame(genomeSize, replace = FALSE)

## Subset data per species to use to calculate averages
GRA <- subset.data.frame(GS, GS$Species == "GRA")
DUN <- subset.data.frame(GS, GS$Species == "DUN")
GU <- subset.data.frame(GS, GS$Species == "GU")
GUxU <- subset.data.frame(GS, GS$Species == "GUxU")
URO <- subset.data.frame(GS, GS$Species == "URO")

## Find average of columns for subset
avghtzGra <- mean(GRA[,"AVGHeterozygosity"])
avgULGra <- mean(GRA[,"MaxUniqueLength"])
avgRLGra <- mean(GRA[,"MaxHaploidLength"])

avghtzDUN <- mean(DUN[,"AVGHeterozygosity"])
avgULDUN <- mean(DUN[,"MaxUniqueLength"])
avgRLDUN <- mean(DUN[,"MaxHaploidLength"])

## Add column to dataframe for fill colour or axis label colour
GS$colour<-NA
GS[GS$Species =="GRA",]$colour<-"#00B050"
GS[GS$Species =="DUN",]$colour<-"#B342B3"
GS[GS$Species =="URO",]$colour<-"#1e6cd1"
GS[GS$Species =="GU",]$colour<-"#04CBCB"
GS[GS$Species =="GUxU",]$colour<-"#183A37"

##For legend
cols <- c("GRA" = "#00B050", "DUN" = "#B342B3", "URO" = "#1e6cd1", "GU" = "#04CBCB", "GUxU"= "#183A37")


## Use this just to get legend for later
a <- ggplot(GS, aes(x = Sample, y = AVGHeterozygosity, fill = Species, group = Species)) +
  geom_col() +
  ylab("Heterozygosity (%)") +
  scale_x_discrete() +
  expand_limits(y=5) +
  scale_y_continuous(expand = c(0, 0)) +
  scale_discrete_manual(aesthetics = c("fill"), values = cols, labels = c(expression(italic("E. dunnii")), expression(italic("E. grandis")), "GU", "GUxU", expression(italic("E. urophylla")))) +
  theme(panel.background = element_blank(), panel.border = element_blank(),
        panel.grid.major.x = element_blank(), 
        panel.grid.minor.y = element_line(colour = "grey95"), panel.grid.major.y = element_line(colour = "grey85"),
        axis.ticks.x = element_blank(), axis.text.x = element_text(size = 12, face="plain", vjust = 0.4, angle = 90, colour = "black"),
        axis.ticks.y = element_line(size = 0.5, color = "grey85"), axis.text.y = element_text(colour = "black", size = 12, face = "plain"),
        legend.text = element_text(size=12), legend.title = element_text(size = 14),
        legend.text.align = 0, legend.position = c(0.95,0.95),  # Modify this if the legend is covering your favorite circle
        legend.background = element_rect(size=0.5, linetype="solid", colour ="black"),
        legend.box.just = "right",
        legend.justification = c("right", "top"),axis.title=element_text(size=12,face="bold"),
        axis.text=element_text(size=12), axis.line = element_line(colour = "grey85"),
        plot.background = element_blank()) +
  geom_hline(yintercept = avghtzGra, linetype = "solid", colour = "black", size = 0.5) +
  geom_hline(yintercept = avghtzDUN, linetype = "dashed", colour = "black", size = 0.5)



## Make plot for average htz  
#GS[which(GS$AVGHeterozygosity>0),] - select only rows that have a value (so when NA is converted to 0, select those with actual values)
ggplot(GS[which(GS$AVGHeterozygosity>0),], aes(x = Sample, y = AVGHeterozygosity, fill = Species, group = Species)) +
  geom_col() +
  ylab("Heterozygosity (%)") +
  expand_limits(y=5) + ## Adjust labels to show up to 5% 
  scale_y_continuous(expand = c(0, 0)) + 
  scale_x_discrete(limits=c("AP928", "AP929", "AP923", "AP924", "AP932", "AP926", "AP921", 
                            "AP927", "AP931",  "AP922", "AP925", "AP962", 
                            "AP966", "AP967", "AP939", "AP959", "AP968", "AP965", "AP964", 
                            "AP960", "AP930", "H1701", "P1381", "FK1752", "FK1758", "A0380","FK118", "NN2868", 
                            "FK1753", "NN0784", "M1459", "FK1755", "FK1756",
                            "BV174", "BV143", "BV164", "BV157", "BV100", "BV139",
                            "BV170", "BV175", "BV138", "BH1697", "BH1477", "BV155", "BH528", 
                            "BH762", "BH840")) + ##This is a custom order of samples
  scale_discrete_manual(aesthetics = c("fill"), values = cols, labels = c(expression(italic("E. dunnii")), expression(italic("E. grandis")), "GU", "GUxU", expression(italic("E. urophylla")))) + ##Customize legend so that Species is italic
  theme_bw() +
  theme(panel.background = element_blank(), panel.border = element_blank(), ##make no block around plot
        panel.grid.major.x = element_blank(), 
        panel.grid.minor.y = element_line(colour = "grey95"), panel.grid.major.y = element_line(colour = "grey85"),
        axis.ticks.x = element_blank(), axis.text.x = element_text(size = 12, face="plain", vjust = 0.4, angle = 90, colour = GS$colour, hjust = 1), ## use colour = GS$Colour to colour axis text by column created at start, I assume this is possible for y too  
        axis.ticks.y = element_line(size = 0.5, color = "grey85"), axis.text.y = element_text(colour = "black", size = 12, face = "plain"),
        axis.title.x = element_text(size=14,face="bold", vjust = -1.5),
        axis.title.y = element_text(size=14,face="bold", vjust = 4),
        axis.text=element_text(size=14), axis.line = element_line(colour = "grey85"),
        legend.position = "none", ## Plot no legend
        plot.background = element_blank(), plot.margin = unit(c(0.5,0.5,0.5,0.5), "cm")) +
  geom_hline(yintercept = avghtzGra, linetype = "solid", colour = "black", size = 1) +
  geom_label(x = 6.5, y = 2.15,label = expression(paste("Average ", italic("E. grandis"), " = 2.02%")), fill = "#FFFFFF08", label.padding = unit(0.2, "lines"), label.size = NA, size = 5, fontface = "bold") +
  geom_hline(yintercept = avghtzDUN, linetype = "dashed", colour = "black", size = 1)+
  geom_label(x = 43, y = 1.85, label = expression(paste("Average ", italic("E. dunnii"), " = 1.96%")), fill = "#FFFFFF08", label.padding = unit(0.2, "lines"), label.size = NA, size = 5, fontface = "bold") # change last 2 values of hex code for transparency

## in axis.text.x or y = element.text() use colour = GS$Colour to colour axis text by column created at start, I assume this is possible for y too
#colour = c("#FFFFFF", "#XXXXXX") can also be used, but needs to be the same number of colours as labels and needs the same order as given in scale_x_discrete

##Alternatively one can label hlines with but these have no box 
#annotate(geom="text", x = 2, y = 2.2, label = "2.02%", colour = "black") 
#geom_text(aes(0,avghtzGra,label = "2.02%", vjust = -0.5, hjust = -0.2)) 

## Label hlines with box using geom_label,  adjust x and y for box position
#NOTE: label="My label" when plain and use label = expression(paste("Average ", italic("E. dunnii"), " = 1.96%")) to get mixed
#label.padding is space around text in label
#label.size = NA removes border arounf label box
#when fill = "#FFFFFF08", the last two digits changes the labels transparency
  
## Plot stacked bar chart for repeat length and unique length to get haploid length
#Select subsets of data for dataframe for stacked chart
#Requires lengths in 1 column and the type of length in a seperate one, not 2 length columns
RepeatLength <- subset.data.frame(GS, select = c(1,9,17))
UniqueLength <- subset.data.frame(GS, select = c(1,12,17))


##Create dataframe for stacked chart
hap <- data.frame(Sample=RepeatLength$Sample, Size = RepeatLength$MaxRepeatLength, species = RepeatLength$Species, Type = c("Repeat Length"))
hap2 <- data.frame(Sample=UniqueLength$Sample, Size = UniqueLength$MaxUniqueLength, species = UniqueLength$Species, Type = c("Unique Lenght"))

##Combine created dataframes
haplength <- rbind(hap, hap2)
 
## Add column to dataframe for fill colour or axis label colour
haplength$colour<-NA
haplength[haplength$species =="GRA",]$colour<-"#00B050"
haplength[haplength$species =="DUN",]$colour<-"#B342B3"
haplength[haplength$species =="URO",]$colour<-"#1e6cd1"
haplength[haplength$species =="GU",]$colour<-"#04CBCB"
haplength[haplength$species =="GUxU",]$colour<-"#183A37"

##Again only for legend
b <- ggplot(haplength, aes(x = Sample, y = Size, fill = Type)) +
  geom_col() +
  ylab("Size (Mb)") +
  scale_x_discrete() +
  scale_y_continuous(expand = c(0, 0), breaks = seq(0,650000000, by = 100000000), labels = addUnits) +
  theme_bw() +
  theme(panel.background = element_blank(), panel.border = element_blank(),
        panel.grid.major.x = element_blank(), 
        panel.grid.minor.y = element_line(colour = "grey95"), panel.grid.major.y = element_line(colour = "grey85"),
        axis.ticks.x = element_blank(), axis.text.x = element_text(size = 12, face="bold", vjust = 0.4, hjust = 1, angle = 90, colour = haplength$colour),
        axis.ticks.y = element_line(size = 0.5, color = "grey90"), axis.text.y = element_text(colour = "black", size = 12, face = "plain"),
        legend.text = element_text(size=12), legend.title = element_text(size = 14),
        legend.text.align = 0,
        legend.background = element_rect(size=0.5, linetype="solid", colour ="black"),
        legend.box.just = "right",
        axis.title=element_text(size=14,face="bold"), 
        axis.text=element_text(size=14), axis.line = element_line(colour = "grey85"),
        plot.background = element_blank()) +
  geom_hline(yintercept = avgULGra, linetype = "solid", colour = "black", size = 0.5) +
  geom_text(aes(0,avgULGra,label = "305.24 Mb", vjust = -0.3, hjust = -0.1)) +
  geom_hline(yintercept = avgULDUN, linetype = "dashed", colour = "black", size = 0.5) +
  geom_text(aes(0,avgULDUN,label = "302.88 Mb", vjust = 1.2, hjust = -0.1)) +
  geom_hline(yintercept = avgRLGra, linetype = "solid", colour = "black", size = 0.5) +
  geom_text(aes(0,avgRLGra,label = "494.95 Mb", vjust = -0.3, hjust = -0.1)) +
  geom_hline(yintercept = avgRLDUN, linetype = "dashed", colour = "black", size = 0.5) +
  geom_text(aes(0,avgRLDUN,label = "476.29 Mb", vjust = 1.2, hjust = -0.1)) 
  
 
##Plot stacked bar chart
ggplot(haplength, aes(x = Sample, y = Size, fill = Type)) +
  geom_bar(stat = "identity") +
  ylab("Size (Mb)") +
  scale_x_discrete(limits=c("AP928", "AP929", "AP923", "AP924", "AP932", "AP926", "AP921", 
                            "AP927", "AP931", "AP922", "AP925",  "AP962", 
                            "AP966", "AP967", "AP939", "AP959", "AP968", "AP965", "AP964", 
                            "AP960", "AP930", "H1701", "P1381", "FK1752", "FK1758", "A0380",  
                            "FK1753", "FK118", "NN2868", "NN0784", "M1459", "FK1755", "FK1756",
                            "BV174", "BV143", "BV164", "BV157", "BV100", "BV139",
                            "BV170", "BV175", "BV138", "BH1697", "BH1477", "BV155", "BH528", 
                            "BH762", "BH840")) + ##This is a custom order of samples
  scale_y_continuous(expand = c(0, 0), breaks = seq(0,650000000, by = 100000000), labels = addUnits) + ## Adjust labels to show up to 650 Mb and have labels at 100Mb intervals
  theme_bw() +
  theme(panel.background = element_blank(), panel.border = element_blank(),
        panel.grid.major.x = element_blank(), 
        panel.grid.minor.y = element_line(colour = "grey95"), panel.grid.major.y = element_line(colour = "grey85"),
        axis.ticks.x = element_blank(), axis.text.x = element_text(size = 12, face="plain", vjust = 0.4, hjust = 1, angle = 90, colour = haplength$colour),
        axis.ticks.y = element_line(size = 0.5, color = "grey90"), axis.text.y = element_text(colour = "black", size = 12, face = "plain"),
        axis.title.x = element_text(size=14,face="bold", vjust = -1.5),
        axis.title.y = element_text(size=14,face="bold", vjust = 4),
        axis.title=element_text(size=14,face="bold"),
        axis.text=element_text(size=14), axis.line = element_line(colour = "grey85"),
        legend.position = "none", plot.margin = unit(c(0.5,0.5,0.5,0.5), "cm"),
        plot.background = element_blank()) +
  geom_hline(yintercept = avgULGra, linetype = "solid", colour = "black", size = 0.5) +
  geom_label(x = 9, y = 321000000,label = expression(paste("Average Unique ", italic("E. grandis"), " = 305.24 Mb")), fill = "#FFFFFF08", label.padding = unit(0.2, "lines"), label.size = NA, size = 5, fontface = "bold") +
  geom_hline(yintercept = avgULDUN, linetype = "dashed", colour = "black", size = 0.5) +
  geom_label(x = 8.8, y = 286000000,label = expression(paste("Average Unique ", italic("E. dunnii"), " = 302.89 Mb")), fill = "#FFFFFF08", label.padding = unit(0.2, "lines"), label.size = NA, size = 5, fontface = "bold") +
  geom_hline(yintercept = avgRLGra, linetype = "solid", colour = "black", size = 0.5) +
  geom_label(x = 9, y = 513000000,label = expression(paste("Average Haploid ", italic("E. grandis"), " = 494.95 Mb")), fill = "#FFFFFF08", label.padding = unit(0.2, "lines"), label.size = NA, size = 5, fontface = "bold") +
  geom_hline(yintercept = avgRLDUN, linetype = "dashed", colour = "black", size = 0.5) +
  geom_label(x = 8.8, y = 458000000,label = expression(paste("Average Haploid ", italic("E. dunnii"), " = 476.29 Mb")), fill = "#FFFFFF08", label.padding = unit(0.2, "lines"), label.size = NA, size = 5, fontface = "bold") 

## in axis.text.x or y = element.text() use colour = haplength$Colour to colour axis text by column created at start, I assume this is possible for y too
#colour = c("#FFFFFF", "#XXXXXX") can also be used, but needs to be the same number of colours as labels and needs the same order as given in scale_x_discrete

## Alternatively one can label hlines with but these have no box 
#annotate(geom="text", x = 2, y = 2.2, label = "2.02%", colour = "black") 
#geom_text(aes(0,avghtzGra,label = "2.02%", vjust = -0.5, hjust = -0.2)) 

## Label hlines with box using geom_label,  adjust x and y for box position
#NOTE: label="My label" when plain and use label = expression(paste("Average ", italic("E. dunnii"), " = 1.96%")) to get mixed
#label.padding is space around text in label
#label.size = NA removes border arounf label box
#when fill = "#FFFFFF08", the last two digits changes the labels transparency


## Get legends from previous two plots
legenda <- cowplot::get_legend(a)
legendb <- cowplot::get_legend(b)

## Make legends as plots
as_ggplot(legenda)
as_ggplot(legendb)

## Draw legends as plots on 1 page
grid.newpage()
grid.draw(legenda)
grid.draw(legendb) 



subset <- read_xlsx("G:/My Drive/2019 MSc - Anneri Lotter/PhD/GenomeScope/Subset.xlsx")

ggplot(subset, aes(x = Sample, y = AVGHeterozygosity, fill = Species, group = Species)) +
  geom_bar() +
  ylab("Heterozygosity (%)") +
  expand_limits(y=5) +
  scale_y_continuous(expand = c(0, 0)) + 
  scale_x_discrete(limits=c("FK1752", "FK1758", "FK1753", "FK118", "FK118_Sub", "FK1755", "FK1755_Sub",
                            "FK1756", "FK1756_Sub", "NN2868", "NN0784")) +
  theme_bw() +
  theme(panel.background = element_blank(), panel.border = element_blank(),
        panel.grid.major.x = element_blank(), 
        panel.grid.minor.y = element_line(colour = "grey95"), panel.grid.major.y = element_line(colour = "grey85"),
        axis.ticks.x = element_blank(), axis.text.x = element_text(size = 12, face="bold", vjust = 0.4, hjust = 0.5, angle = 90, colour = "black"),
        axis.ticks.y = element_line(size = 0.5, color = "grey90"), axis.text.y = element_text(colour = "black", size = 12, face = "plain"),
        legend.text = element_text(size=12), legend.title = element_text(size = 14),
        axis.title=element_text(size=14,face="bold"),
        axis.text=element_text(size=14), axis.line = element_line(colour = "grey85"),
        plot.background = element_blank()) +
  geom_hline(yintercept = avghtzGra, linetype = "solid", colour = "black", size = 0.5) 

RepeatLengthSub <- subset.data.frame(subset, select = c(1,9,17))
UniqueLengthSub <- subset.data.frame(subset, select = c(1,12,17))



sub1 <- data.frame(Sample=RepeatLengthSub$Sample, Size = RepeatLengthSub$MaxRepeatLength, species = RepeatLengthSub$Species, Type = c("Repeat Length"))
sub2 <- data.frame(Sample=UniqueLengthSub$Sample, Size = UniqueLengthSub$MaxUniqueLength, species = UniqueLengthSub$Species, Type = c("Unique Lenght"))


suball <- rbind(sub1, sub2)


suball$colour<-NA
suball[suball$species =="GRA",]$colour<-"#00B050"
suball[suball$species =="URO",]$colour<-"#1e6cd1"
suball[suball$species =="GU",]$colour<-"#04CBCB"
suball[suball$species =="GUxU",]$colour<-"#183A37"

ggplot(suball, aes(x = Sample, y = Size, fill = Type)) +
  geom_col(stat = "identity") +
  ylab("Size (Mb)") +
  scale_x_discrete(limits=c("FK1752", "FK1758", "FK1753", "FK118", "FK118_Sub",  "NN2868", "NN0784", "FK1755", "FK1755_Sub", 
                            "FK1756", "FK1756_Sub")) +
  scale_y_continuous(limits = c(0, 650000000), expand = c(0, 0), breaks = seq(0,650000000, by = 100000000), labels = addUnits) +
  theme_bw() +
  theme(panel.background = element_blank(), panel.border = element_blank(),
        panel.grid.major.x = element_blank(), 
        panel.grid.minor.y = element_line(colour = "grey95"), panel.grid.major.y = element_line(colour = "grey85"),
        axis.ticks.x = element_blank(), axis.text.x = element_text(size = 12, face="bold", vjust = 0.4, hjust = 0.5, 
                                                                   angle = 90, colour = c("#00B050", "#00B050", "#04CBCB","#04CBCB", "#04CBCB", "#04CBCB", "#183A37", "#1e6cd1", "#1e6cd1", "#1e6cd1", "#1e6cd1")),
        axis.ticks.y = element_line(size = 0.5, color = "grey90"), axis.text.y = element_text(colour = "black", size = 12, face = "plain"),
        axis.title.x = element_text(size=14,face="bold", vjust = -1.5),
        axis.title.y = element_text(size=14,face="bold", vjust = 4),
        axis.title=element_text(size=14,face="bold"),
        axis.text=element_text(size=14), axis.line = element_line(colour = "grey85"),
        legend.position = "none", plot.margin = unit(c(0.5,0.5,0.5,0.5), "cm"),
        plot.background = element_blank()) +
  geom_hline(yintercept = avgULGra, linetype = "solid", colour = "black", size = 0.5) +
  geom_label(x = 2.25, y = 321000000,label = expression(paste("Average Unique ", italic("E. grandis"), " = 305.24 Mb")), fill = "#FFFFFF08", label.padding = unit(0.2, "lines"), label.size = NA, size = 5, fontface = "bold") +
  geom_hline(yintercept = avgULDUN, linetype = "dashed", colour = "black", size = 0.5) +
  geom_label(x = 2.2, y = 286000000,label = expression(paste("Average Unique ", italic("E. dunnii"), " = 302.89 Mb")), fill = "#FFFFFF08", label.padding = unit(0.2, "lines"), label.size = NA, size = 5, fontface = "bold") +
  geom_hline(yintercept = avgRLGra, linetype = "solid", colour = "black", size = 0.5) +
  geom_label(x = 2.25, y = 513000000,label = expression(paste("Average Haploid ", italic("E. grandis"), " = 494.95 Mb")), fill = "#FFFFFF08", label.padding = unit(0.2, "lines"), label.size = NA, size = 5, fontface = "bold") +
  geom_hline(yintercept = avgRLDUN, linetype = "dashed", colour = "black", size = 0.5) +
  geom_label(x = 2.2, y = 458000000,label = expression(paste("Average Haploid ", italic("E. dunnii"), " = 476.29 Mb")), fill = "#FFFFFF08", label.padding = unit(0.2, "lines"), label.size = NA, size = 5, fontface = "bold") 

  
  
