#!/bin/bash
#SBATCH --job-name=bwa
#SBATCH -n 1
#SBATCH -N 1
#SBATCH -c 8
#SBATCH --mem=150G
#SBATCH --partition=himem
#SBATCH --qos=himem
#SBATCH --mail-type=END
#SBATCH --mail-user=anneri.lotter@fabi.up.ac.za
#SBATCH -o %x_%A.out
#SBATCH -e %x_%A.err

#module load bwa/0.7.5a
#bwa index ../final.genome.scf.fasta
#bwa mem -t 8 ../final.genome.scf.fasta /projects/EBP/CBC/eucalyptus/rawReads/Illumina/GRA_FK1758_unclassified_1.fastq /projects/EBP/CBC/eucalyptus/rawReads/Illumina/GRA_FK1758_unclassified_2.fastq > grandis_map_nanopore.sam

module load samtools/1.9
samtools view -S -b grandis_map_nanopore.sam > grandis_map_nanopore.bam
samtools index -b grandis_map_nanopore.bam
samtools flagstat grandis_map_nanopore.bam > stats.out
