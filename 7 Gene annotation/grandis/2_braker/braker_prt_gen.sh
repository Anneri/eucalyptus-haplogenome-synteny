#!/bin/bash
#SBATCH --job-name=braker
#SBATCH -N 1
#SBATCH -n 1
#SBATCH -c 10
#SBATCH --partition=general
#SBATCH --qos=general
#SBATCH --mail-type=END
#SBATCH --mem=150G
#SBATCH --mail-user=anneri.lotter@fabi.up.ac.za
#SBATCH -o %x_%j.out
#SBATCH -e %x_%j.err

module load BRAKER/2.0.5 
module load augustus/3.2.3 
module load bamtools/2.4.1 

export PATH=/home/FCAM/$USER/BRAKER:/home/FCAM/$USER/BRAKER/scripts:$PATH 
export AUGUSTUS_CONFIG_PATH=$HOME/augustus-3.2.3/3.2.3/config
#export AUGUSTUS_BIN_PATH=$HOME/augustus-3.2.3/3.2.3/bin
export TMPDIR=/core/projects/EBP/CBC/eucalyptus/rawReads/Nanopore/gene_annotation/gra/hisat2/braker/tmp
export BAMTOOLS_PATH=/isg/shared/apps/bamtools/2.5.1/bin/
#the path to the locally installed BRAKER
export BLAST_PATH=/isg/shared/apps/blast/ncbi-blast-2.10.0+/bin/
export GENEMARK_PATH=/isg/shared/apps/GeneMark-ET/4.38
export SAMTOOLS_PATH=/isg/shared/apps/samtools/1.9/bin/
export ALIGNMENT_TOOL_PATH=/isg/shared/apps/gth/1.7.1/bin/
#export GENEMARK_PATH=/isg/shared/apps/GeneMark-ET/4.59

cp ~/local_gm_key_64 ~/.gm_key

module load perl/5.24.0
export PERL5LIB=/core/labs/Wegrzyn/perl5/lib/perl5/
export PERLINC=/core/labs/Wegrzyn/perl5/lib/perl5/


#braker.pl --cores 20 --genome=gra_final_masked.fasta --species=grandisumfinal2 --bam allBamMerged.bam --softmasking 1 --gff3 --AUGUSTUS_CONFIG_PATH=$HOME/augustus-3.2.3/3.2.3/config
braker.pl --cores 10 \
	--genome=../gra_final_masked.fasta \
	--bam ../allBamMerged.bam \
	--softmasking 1 \
	--prot_seq=GCF_000612305.1_Egrandis1_0_protein.faa \
	--prg=gth --gth2traingenes \
	--gff3 \
	--AUGUSTUS_ab_initio
