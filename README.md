# Haplogenome synteny of _Eucalyptus urophylla_ and _E. grandis_ 

[Data tracking sheet](https://docs.google.com/spreadsheets/d/1CExG0JOI0iO5IvEVQBJexyIM6ncKUYtlV5GaCt7qhXs/edit?usp=sharing)

# F1 _Eucalyptus grandis_ x _E. urophylla_

NCBI BioProject: [PRJNA885070](https://www.ncbi.nlm.nih.gov/bioproject/885070)\
Data: /archive/labs/wegrzyn/genomes/eucalyptus/2019_MSc\
Illumina PE 150bp: 178.6X coverage\
Nanopore: 105.4X\
Size estimation in GenomeScope - 425,203,321 bp

## _Eucalyptus urophylla_ maternal 

NCBI BioProject: [PRJNA874516](https://www.ncbi.nlm.nih.gov/bioproject/PRJNA874516/)\
Data: /archive/labs/wegrzyn/genomes/eucalyptus/2019_MSc\
Illumina PE 150bp: 196.2X coverage\
Nanopore: 50X coverage\
Size estimation in GenomeScope - 412,295,044 bp

## _Eucalyptus grandis_ paternal

NCBI BioProject: [PRJNA870669](https://www.ncbi.nlm.nih.gov/bioproject/PRJNA870669/)\
Data: /archive/labs/wegrzyn/genomes/eucalyptus/2019_MSc\
Illumina PE 150bp: 217.8X coverage\
Nanopore: 54X coverage\
Size estimation in GenomeScope - 428,673,422bp

## **Project background**

Current breeding strategies for eucalypt tree improvement are less accurate than haplotype-based molecular breeding strategies being deployed in other crop species. However, to use haplotype-based molecular breeding strategies for tree improvement, a high-quality reference genome is required for the species. The current *E. grandis* genome has been assembled using short-read technologies, which pose a variety of challenges and result in fragmented and incomplete reference genomes. Towards this, long-read sequencing technologies offer a solution to many of these challenges, and can be used to improve / assemble high quality reference genomes. In addition, long-read sequencing has been used in combination with short-read sequencing, to obtain high quality reference genomes for two parental species by sequencing an interspecific hybrid. This reduces the cost of generating such assemblies, as fewer sequence information is required than when these genomes are assembled independently.

As such, this project aims to assemble the genome of an F1 *E. urophylla* x *E. grandis* hybrid, to assemble high-quality reference genomes for both *E. urophylla* and *E. grandis*.

In order to obtain the best possible genome, we tested a trio-binning approach for haplotype assembly of *E. grandis* and *E. urophylla* as discussed in a paper by Koren *et al* 2018.

[TOC]

## [**1) DNA Isolation**](https://gitlab.com/Anneri/eucalyptus-haplogenome-synteny/-/tree/main/1%20DNA%20Isolation)

#### **Illumina sequencing**

DNA for short-read sequencing was extracted using a modified [Machery-Nagel NucleoSpin® Plant II protocol](1 DNA Isolation/DNA_extractions_MN_kit_SOP_v1_27July2017.docx). 

#### **Nanopore sequencing**

DNA for Nanopore sequencing was extracted using the [Qiagen 100G-Tip](1 DNA Isolation/100G-Tip.md) for MinION sequencing after which DNA extracted with the 100G-Tip was sent for PromethION sequencing.


## **2) DNA Sequencing**

DNA was extracted from leaf tissue of and F1 *E. urophylla* x *E. grandis* hybrid. Sequencing of the hybrid
was performed using the ONT MinION and PromethION v9.4 platform.

Additionally, to use the trio-binning approach proposed by Koren *et al*, 151 bp PE Illumina sequencing data was generated for the F1 hybrid, as well as the *E. urophylla* and *E. grandis* parents.

Raw long-read sequencing data, base-called long-read sequencing, contaminant removed binned long reads, raw short read sequencing data and trimmed short-read sequencign data is available upon request if not available on NCBI using the above links.

**Illumina sequencing data:**
*	*E. urophylla* parent: 127.5 Gb, 196.2x
*	*E. grandis* parent: 141.6 Gb, 217.8x
*	F1 Hybrid: 116.1 Gb, 178.61x

**Nanopore sequencing data:**
*	MinION Run1: 11.18 Gb called, 9.5 Gb passed
*	MinION Run2: 2.55 Gb called, 2.32 Gb passed
*	PromethION: 61.59 Gb called, 56.69 Gb passed
*	Total: 75.32 Gb called, 68.51 Gb passed

Total estimated ONT coverage: 105.4x


## [**3) Read basecalling and QC**](https://gitlab.com/Anneri/eucalyptus-haplogenome-synteny/-/tree/main/3%20Read%20basecalling%20and%20QC)
 
#### **Illumina sequencing data**

Basic Illumina read QC was performed with **FastQC**, note that raw data was used as input for read binning with **Canu**.

Genome size was estimated for the F1 hybrid and both parents based on short-read K-mer analysis using **Jellyfish v2.0** and visualizing with **GenomeScopev2.0**.

**Input (FastQC):**
- Raw **fastq** files
- [FASTQC.sh](3 Read basecalling and QC/FASTQC.sh) script to run FASTQC

**Output:**
* FASTQC report file for each read set

**Input (Jellyfish):**
* Raw **fastq** files
* [jellyfish.sh](3 Read basecalling and QC/jellyfish.sh) script to run jellyfish

**Output:**
* **.histo** file containing histogram input for GenomeScope

GenomeScope results can be viewed here for the following individuals:

*	[F1 hybrid](http://qb.cshl.edu/genomescope/genomescope2.0/analysis.php?code=OD5jAu9BCy7MoIFHmkOO)
*	[*E. urophylla* parent](http://qb.cshl.edu/genomescope/genomescope2.0/analysis.php?code=mXVEIQxHMIpGFqh7gzk5)
*	[*E. grandis* parent](http://qb.cshl.edu/genomescope/genomescope2.0/analysis.php?code=cb6smCH0wSyDxu7dpSGu)

Using the .histo file as input for [genomescope2](http://qb.cshl.edu/genomescope/genomescope2.0/), use the default parameters and only input the k-mer length and ploidy (n = 2 for *Eucalyptus*).

#### **ONT sequencing data**

Basecalling was performed using the **Guppy basecaller** (GPU version). This was followed with removal of sequencing adapters was performed using **PoreChop**. 

**Input (Guppy):**
* **FAST5** read files from ONT sequencing
* [guppy.sh](3 Read basecalling and QC/guppy.sh) script to run Guppy 

**Output:**
* **fastq** read files sorted according to passed and failed read QC value (greater than 7)

**Input (PoreChop):**
* **fastq** read files that passed QC (use cat to put all fastq files from guppy into a single fastq file for further use)
* [porechop.sh](3 Read basecalling and QC/porechop.sh) scropt to run porechop

**Output:**
* **fastq** read files with removed adapters


## [**4) Read binning contaminant removal**](https://gitlab.com/Anneri/eucalyptus-haplogenome-synteny/-/tree/main/4%20Read%20binning%20and%20contaminant%20removal)

#### **Trio-binning of F1 ONT reads**

Binning of ONT reads of the F1 hybrid into *E. urophylla* and *E. grandis* bins was performed using [**Canu**]( https://canu.readthedocs.io/en/latest/quick-start.html)

**Input:**
* Raw **fastq** Illumina short read files of parents
* Raw **fastq** ONT sequencing data file of F1 hybrid (all porechoppedd data concatenated into a single fastq file)
* [canu.sh](4 Read binning and contaminant removal/canu.sh) script to bin reads

**Output:**
* Binned **fastq** long-read files

Removal of contaminant reads was performed using **Kraken2** (Illumina) and **Centrifuge** (ONT).

**Input (Kraken2):**
*	PE Illumina reads short reads in **fastq** format
*	Kraken classification database 
*	[kraken.sh](4 Read binning and contaminant removal/kraken.sh) script for generating unclassified and classified read sets

**Output:**
*	Classified and unclassified short read data for each read set (we will want to use the unclassified set going forward)

**Input (Centrifuge):**
*	Raw long reads in **fastq** format
*	Sequence index database of possible contaminants (human, archaea, bacterial and fungal)
*	[centrifuge.sh](4 Read binning and contaminant removal/centrifuge.sh) script to generate a classification report

**Output:**
*	Centrifuge classification report

**Note:** Additional python scripts must be used to remove contaminated reads identified in the report. 


## [**5) Genome assembly and scaffolding**](https://gitlab.com/Anneri/eucalyptus-haplogenome-synteny/-/tree/main/5%20Genome%20assembly%20and%20scaffolding)

#### Masurca assembly of Canu binned reads

Binned long-reads from Canu were assembled independantly using the Masurca hybrid genome assembler (using uncontaminated parental short reads and binned long-reads of the F1 hybrid). **Two separate assemblies were run from this point on, one per bin, thus the scripts are split into the relevant species subfolder for all further analyses steps (i.e. [_E. grandis_](https://gitlab.com/Anneri/eucalyptus-haplogenome-synteny/-/tree/main/5%20Genome%20assembly%20and%20scaffolding/grandis) and [_E. urophylla_](https://gitlab.com/Anneri/eucalyptus-haplogenome-synteny/-/tree/main/5%20Genome%20assembly%20and%20scaffolding/urophylla)).** 

**Input:**
*	Uncontaminated and binned ONT long reads in **fasta** format
*	Unclassified PE Illumina parental short reads in **fastq** format
*	**config.txt** file specifying input reads and parameters for the Masurca genome assembly run (may need to be modified for your particular genome)
*	**masurca_ONT.sh** generates the assemble.sh file to run the Masurca assembly using your specified parameters and FASTQ files

**Output:**
*	**assemble.sh** script for running the assembly
*	Draft genome assembly named final.genome.scf.fasta in the CA directory

#### Checking assembly size

To check if the assembled genome size is correct (and that the reduced assembly size compared to the estimated size is not due to reads not being assembled), **bwa mem** can be used along with the **flagstat** function from **samtools** to check alignment of raw Illumina reads to the assembled genome.

**Input:**
*	**bwa_align.sh** script to run bwa mem and calculate alignment statistics

**Output:**
*	**.sam** and **.bam** alignments files containg raw Illumina reads aligned to the assembled genome
*	**stats.out** file containing the alignment statistics.

#### **Inferring breakpoints for incorrectly assembled reads**

Breakpoints were inferred for incorrectly assembled contigs with [PolarSTAR](https://github.com/phasegenomics/polar_star) based on support (or lack of support) from long-read data.

**Input:**
*   **config.json** file specifying the location of the binned reads and the genome to be validated
*   **polarstar.sh** script to run polarstar
*   Raw uncontaminated long-reads in **fasta** format
*   Assembly in **fasta** format

**Output:**
*   **new_fasta.fa** containing split contigs

Note: All reads of 3 Kb were removed before genome scaffolding. One can do this either way described below with seqtk or the seqkit tool.

```
seqkit seq -m 3000 your_fasta.fa > 3Kb.fasta
seqtk seq -L 3000 contigs.fasta > 3Kb.fasta 
```

#### **Genome scaffolding**

After genome assembly with Masurca and inference of breakpoints, the genome can be scaffolded using high-density genetic linkage maps using the **ALLMAPS** program. 

To do so the following is required to generate input files for ALLMAPS:

1) Create a file containing SNP names and probes with awk
```
awk '{print $1"\t"$2}' SNP Probes.txt > snp_probe_name.txt
```

2) Convert probe files to .fasta files for use in BLAST
```
awk '{ printf ">%s\n%s\n",$1,$2 }' matched_uro1.txt > matched_uro1.fasta
awk '{ printf ">%s\n%s\n",$1,$2 }' matched_gra1.txt > matched_gra1.fasta
```

3) **BLAST** the SNP probe sequences against the assembled genome to find their genetic position.

**Input:**
*	Draft genome assembly in **fasta** format
*	**blast.sh** script (in relevant directory)

**Output:**
*	Database to blast against, consisting of your assembled genome
*	**.blastp** file containing the following info/headers: qseqid (query seq), sseqid (ref genome), pident (% identity match), length (of alignment), mismatch (number mismatches), gapopen (number gap openings), qstart (query alignment start), qend (query alignment end), sstart (ref alignment start), send (ref alignment end), evalue, bitscore

4) Generate a .csv file containing the scaffold ID, scaffold position (this will correspond to the SNP position on the scaffold), the LG (linkage group to which the scaffold belongs, from the genetic linkage map) and the position (physical position as in the gentic map).
```
awk '{print $1"\t"$2"\t"$9"\t"$10 }' SNP_Genome.blastp > UroSNP_Probes_to_Assembly.txt
awk '{print $3"\t"$1":"$4}' urophylla_map_SNPS.txt > urophylla_map_SNPS_map.txt
awk 'BEGIN { FS = OFS = "\t" } NR == FNR {fn[$1] = $2; next} {print $2, $3, ($1 in fn ? fn[$1] : "FALSE")}' urophylla_map_SNPS_map.txt UroSNP_Probes_to_Assembly.txt | sed 's/%/\t/' > AllMap1.file
tr '\t' ',' < AllMap1.file > UroAllMap1.final.csv
```

Note that I used the SNP probe start position and not the start to end position.

6) Using a text editor, insert the headers as specified on the ALLMAPS github page. In this case we specify Scaffold ID,scaffold position,LG,genetic position.

7) Run ALLMAPS by running the **run.sh** script

**Input:**
*	**.csv** file of genetic linkage maps (preferably both parents as this increase scaffolding rate of contigs)
*	**run.sh** script to run scaffolding
*	Draft genome assembly file in **fasta** format

**Output:**
*	**.bed** file containing the SNP positions for both genetic linkage maps
*	**weight.txt** file containing the weight each genetic map will be given (in this case this will be adjusted depending on the haplotype being assembled so the parental haplotype carries greater weight i.e. pivot map)
*	**fasta** file woth reconstructed chromosome sequences
*	PDF files with alignment visualisations corresponding to reconstructed chromosomes
*	**summary.txt** file containing information regarding number of scaffold anchored, N50 etc.

NOTE: You can also estimate gap lengths and redo the assembly.

#### **Genome assembly quality**

Assembled genomes were evaluated for quality after each step using **BUSCO** to identify the number of core genes present in the assembled genome, and **QUAST** to check genome assembly statistics such as N50, # contigs etc.

All follow this layout:

**BUSCO:** \
`run_BUSCO.py -i /path/to/assembly.fasta -l /path/to/embryophyte/data/embryophyta_odb10 -o /name_of_output_folder -m geno ` OR `-m tran` OR `-m prot`

**QUAST:** \
`quast.py /path/to/assembly.fasta -o Name_of_output_folder`


## [**6) Masking of repetitive elements**](https://gitlab.com/Anneri/eucalyptus-haplogenome-synteny/-/tree/main/6%20Repeatmasking)

This step is recommended before genome annotation. After repetitive elements have been masked one can obtain an estimate of the genome repetitiveness, as well as the type of elements involved in repetitiveness.

The first step in annotation is to softmask the genome. Some of the analysis tools down the line consider this information. 

#### **1) RepeatModeler**

To begin the masking process, we must first use **RepeatModeler** on the filtered data. **RepeatModeler** will output a set of **consensi** files, which are used in **RepeatMasker**.

**Input:**
*	**repeatmodeler_*.sh** script to run repeatmodeler
*	Assembled genome file in **fasta** format

**Output:**
*	A **consensi.fa** file containing repeat element database for masking


#### **2) RepeatMasker**

To mask repeat elements based on the repeat library, we use **RepeatMasker**. In this study, we combined the libraries that were constructed with RepeatModeler using cat. We used a combined library as using only the library constructed for the species resulted in ommision of some elements.

**Input:**
*	Assembled genome in **fasta** format
*	**repeatmasker_*.sh** script to mask the assembled genome
*   **consensi.fa** repeat library (the combined library)

**Output:**
*	Masked genome in **fasta** format
*	**.tbl** file which contains a summary of masking percentage and percentage of element classes for the genome 
*   **.out** file with alignment information

#### **3) LTR_Retriever**

To identify LTR elements and classes, LTR_retriever can be used as it is specifically set up for plant LTR detection which may have been missed or classified as unknown by RepeatModeler.

**Input:**
*   Assembled genome in **fasta** format
*   **ltr_*.sh** script to run the various programs for LTR_Retriever
-   LTR_Retriever needs candidates identified with LTR_Finder and LTRHarvest which is used as an input

**Output:**
*   Multiple **scn** and othe intermediate files that are input files for LTR_Retriever
*   **.tbl** file that contains the total percentage of LTR elements found (similar to that generated by RepeatMasker)
*   **.LTR.distribution.txt** file with the distribution of LTR elements in bins of all scaffolds 
-   This was used to visualize distribution with Circos or R

## [**7) Gene annotation**](https://gitlab.com/Anneri/eucalyptus-haplogenome-synteny/-/tree/main/7%20Gene%20annotation)

#### **Structural Annotation**
In this study we used RNA-Seq data from the *E. grandis* v1.0 genome annotation for structural annotation of the *E. grandis* haplogenome. For the *E. urophylla* haplogenome, data of from the Mizrachi *et al.* 2017 GUxU3yo backcross population. 

**1) Read QC and trimming**

The quality of RNA-Seq reads were checked with **FASTQC**. All reads were trimmed with **Trimmomatic** and only paired reads were used for genome annotation.

**Input:**
*	**trim.sh** script to trim reads
*	RNA-Seq reads in **fastq** format

**Output:**
*	Trimmed paired reads in **fastq** format

**2) Mapping RNA_Seq reads with HISAT**

Trimmed and paired RNA-Seq reads will be mapped to the relevant masked haplogenome using **HISAT2**. 

**Input:**
*	masked genome in **fasta** format
*	trimmed and paired reads in **fastq** format
*	**bamlist.fofn** file containing the names of all BAM alignment files for merging
*   **hisatFinal.sh** or **06_align.sh** script to run all commands

**Output:**
*	multiple **.bam** alignment files
*	single merged and sorted **.bam** file for gene prediction
*   the **.err** file generated should also contain simple statistics such as alignment rates of the RNA-Seq data

**3) Identifying and predicting genes with RNA-Seq data with BRAKER2**

[**BRAKER2**](https://academic.oup.com/bioinformatics/article/32/5/767/1744611) will be used to identify and predict gene models based on our RNA-Seq data. A previous genome annotation can also be used to improve prediction models by provifing the protein fasta file from that annotation. BRAKER2 makes use of [**GeneMark**](http://opal.biology.gatech.edu/GeneMark/) as unsupervised machine learning process to produce gene models without needing RNA-Seq data. After this a supervised machine learning process is trained with gene models provided by GeneMark and the aligned RNA-Seq data. This is done by [**AUGUSTUS**](http://bioinf.uni-greifswald.de/augustus/). 

**Input:**
*   RepeatMasked genome file in **fasta** format
*   Merged final alignments in **bam** format (allBamMerged.bam)
*   **braker_prt_gen.sh** script to run braker with the previous genome annotation file
*   Previous genome annotation protein sequences in **fasta** format
*   **busco_prt.sh** script to give a summary of the initial genome annotation 

**Output:**
*   BRAKER2 generates a directory called braker with multiple output files. The following are of note:
	-	augustus.hints.gtf: Genes predicted by AUGUSTUS with hints from given extrinsic evidence. 
	-	augustus.hints.aa: Protein sequence files in FASTA-format
	-	augustus.hints.codingseq: Coding sequences in FASTA-format.
	-	GeneMark-E*/genemark.gtf: Genes predicted by GeneMark-ES/ET/EP/EP+ in GTF-format. This file will be missing if BRAKER was executed with proteins of close homology and the option --trainFromGth.
	-	braker.gtf: Union of augustus.hints.gtf and reliable GeneMark-EX predictions (genes fully supported by external evidence). In --esmode, this is the union of augustus.ab_initio.gtf and all GeneMark-ES genes. Thus, this set is generally more sensitive (more genes correctly predicted) and can be less specific (more false-positive predictions can be present).
	-	hintsfile.gff: The extrinsic evidence data extracted from RNAseq.bam and/or protein data.
*   A completeness assessment from **BUSCO**

**4) Quality control and filtering with gFACs and Interproscan**

We use [**gFACS**](https://gitlab.com/PlantGenomicsLab/gFACs) to extract viable genes and proteins. 

We combined multiple filters to get a final gene set by: 1) filtering for mono-exonic genes, 2) filtering for multi-exonic genes, 3) filtering mono-exonic genes for only those with protein domains and 4) combining the final mono-exonic and multi-exonic set (**gfacs_other.sh** script).


**Input:**
*	The predicted protein alignment file from BRAKER2 in **gff3** format = **augustus.hint.gff3** 
*	Path to the gFACs.pl script
*	Masked genome in **fasta** format
*	**filtergFACsGeneTable.py** script
*   **gfacs_other.sh** script to run gfacs


**Output:**
*	final_o_prt folder containing
	*	genes.fasta.faa - protein sequences in **fasta** format 
	*	gene_table.txt
	*	gFACs_log.txt 
	*	out.gff3
	*	out.gtf
	*	statistics.txt - final statistics
*	BUSCO completeness statistics

#### **Functional annotation** 

**5) EnTap**

Similar to above, functional Annotation with EnTap was performed for models based on the RNA-Seq data with the v1.0 annotation input and INTERPROSCAN filtering (**09_entap_prt_ipscn.sh**) under the 4_entap/all_o folder. 

**Input:**
*	**09_entap_prt_ipscn.sh** script to run EnTap
*	**entap_config.txt** file to provide path to dependancies
*	Filtered final protein sequences from gFACs in **fasta** format
*	Pathway to relevant databases (-d flag)

**Output:**
*	A folder with the following subfolders and files:
```
├──	final_results --> final annotations
Gene ontology terms are normalized to levels based on the input flag from the user (or the default of 0,3,4). A level of 0 within the filename indicates that ALL GO terms will be printed to the annotation file. Normalization of GO terms to levels is generally done before enrichment analysis and is based upon the hierarchical setup of the Gene Ontology database.	
- final_annotations_lvlX.tsv: X represents the normalized GO terms for the annotation
- final_annotated.faa / .fnn: Nucleotide and protein fasta files containing all sequences that either hit databases through similarity searching or through the ontology stage
- final_unannotated.aa / .fnn: Nucleotide and protein fasta files containing all sequences that did not hit either through similarity searching nor through the ontology stage
├── ontology --> ontology results against EggNOG pipeline 
	├── EggNOG_DMND/
		├── processed/ --> contain annotations
			- annotated_sequences*.tsv/faa
├── similarity_search --> results from the diamond databases included in your search
	├── DIAMOND/  
		├── processed/ --> information based on best hits from similarity searches against databases 
			├──  complete/ 
			- best_hits_contam.faa/.fnn/.tsv will contain contaminants (protein/nucleotide) separated from the best hits file.
			- best_hits_no_contam.faa/.fnn/.tsv will contain sequences (protein/nucleotide) that were selected as best hits and not flagged as contaminants
			- no_hits.faa/.fnn/.tsv contain sequences (protein/nucleotide) from the transcriptome that did not hit against this particular database
			- unselected.tsv will contain result in several hits for each query sequence. With only one best hit being selected, the rest are unselected and end up here
└── transcriptomes
- **.log** file with a summary of the gene assignments and annotations
```

## [**8) Genome synteny**](https://gitlab.com/Anneri/eucalyptus-haplogenome-synteny/-/tree/main/8%20SV)

#### [**Genome wide synteny and structural rearrangements (SyRI)** ](https://gitlab.com/Anneri/eucalyptus-haplogenome-synteny/-/tree/main/8%20SV/1-SyRI) 
Masking of repeat elements is recommended before SV identification in large genomes like wheat to reduce runtimes, however I did not use a masked genome as input for this study. ONLY THE CHROMOSOMES ARE USED AS INPUT.

**Input:**
*   **fasta** files of chromosomes of the genomes to be compared 
*   **XX_syri.sh** script to run [SyRI](https://github.com/schneebergerlab/syri)
*	**plotsr.sh** script to plot 4-way synteny using the SyRI output files

**Output:**
*   Many **txt** files with different SV and local variant calls
*   An **out** file used for plotting results - this file can be used for visualization with Circos
*   A **pdf** file containing a linear alignment of syntenic and rearranged regions (svg can also be the selected output)
*   A **summary** file that summarizes the total number of variants and their total size
*   **.delta** file 
*	**.svg** synteny plots generated with [plotsr](https://github.com/schneebergerlab/plotsr/tree/master/plotsr)

#### [**Gene based synteny (MCScan_py)**](https://gitlab.com/Anneri/eucalyptus-haplogenome-synteny/-/tree/main/8%20SV/2-MCScan_py)

To run MCScan_py, one first needs to blast the protein.fasta files agains one another. However, proteins that are in unscaffolded regions complicate things, so we first remove them. In addition we rename the gene ID's to make them specific for each haplotype.

##### **1) Getting the files ready**

Using the output fasta file from gfacs (named *genes.fasta.faa* and *out.gtf* and renamed per haplotype) we separate only the chromosome annotations (the expected file output is also given):
```
grep "^chr" eg.genes.gtf > eg.genes_onlyChr.gtf
grep "^chr" eu.genes.gtf > eu.genes_onlyChr.gtf
```

Then to make the comparison easier to follow, we rename ID's based on the haplotype:

```
grep -P "\tgene\t" eu.genes_onlyChr.gtf | sed 's/ID=//;s/\;//' | sed "s/^chr/EU/" | awk '{print $1"\t"$1$9"\t"$4"\t"$5}' > eur_OnlyChr.gff
grep -P "\tgene\t" eg.genes_onlyChr.gtf | sed 's/ID=//;s/\;//' | sed "s/^chr/EG/" | awk '{print $1"\t"$1$9"\t"$4"\t"$5}' > egr_OnlyChr.gff
```

```
grep -P "\tgene\t" eu.genes_onlyChr.gtf | sed 's/ID=//;s/\;//' | sed "s/^chr/EU/" | awk '{print $9"\t"$1$9}' > eu_renamed.map
grep -P "\tgene\t" eg.genes_onlyChr.gtf | sed 's/ID=//;s/\;//' | sed "s/^chr/EG/" | awk '{print $9"\t"$1$9}' > eg_renamed.map
grep '>' eg.genes.aa | sed 's/>//' > egr.only.1
grep '>' eu.genes.aa | sed 's/>//' > eur.only.1
```

We then use the [*faSomeRecords.py*](https://raw.githubusercontent.com/santiagosnchez/faSomeRecords/master/faSomeRecords.py) script to get the fasta sequences of the ID's above from the protein.fasta file

```
python3 faSomeRecords.py -f eg.genes.aa -l egr.only.1 --outfile egr.genes_only.1.faa
python3 faSomeRecords.py -f eu.genes.aa -l eur.only.1 --outfile eur.genes_only.1.faa
```

We change the ID= in the *.genes_only.1.faa* file to just g with sed
```
sed -i 's/ID=//g' eur.genes_only.1.faa
sed -i 's/ID=//g' egr.genes_only.1.faa
```

And rename g### to EG#g### with maker
```
module load maker/2.31.9
map_fasta_ids eg_renamed.map egr.genes_only.1.faa
map_fasta_ids eu_renamed.map eur.genes_only.1.faa
grep '>' eur.genes_only.1.faa | grep ">EU" | sed 's/>//' > EUOnlyChrGeneID
grep '>' egr.genes_only.1.faa | grep ">EG" | sed 's/>//' > EGOnlyChrGeneID
```

Again we use *faSomeRecords.py* to rename the fasta sequences:
```
python3 faSomeRecords.py -f egr.genes_only.1.faa -l EGOnlyChrGeneID --outfile eg.protein.faa
python3 faSomeRecords.py -f eur.genes_only.1.faa -l EUOnlyChrGeneID --outfile eu.protein.faa
```

##### **2) Running MCScan_py**

**Input:**
* **MCScan_py_cXX.sh** script to run MCScan_py with the specified Cscore. The higher the Cscore, the more closely related we expect the species to be and the greater the filter for identity between the two gene annotations.
* **blocks.layout** file containing that tells the plotter what to draw, where to draw and what .bed files to use
* **seqids** file to specify which chromosomes to plot and what order
* **layout** file to specify labels, their position and colour

**Output:**
* **.anchors** file containing last alignments (synteny blocks) of gene or protein sequences which can be filtered using Cscore parameter for all remaining plots
* A dotplot alignment of gene pairs (default shows 10 000 random alignments but can be altered to show more) 
* A historgram showing the synteny pattern (1:1 in this case)
* **.simple** file which is a Cscore filtered anchors file with synteny blocks (can change minspan parameter to set the minimum gene block size). This file can be altered to change the colour of specific synteny blocks.
* A macrosynteny plot


## [**9) Supplementary notes**](https://gitlab.com/Anneri/eucalyptus-haplogenome-synteny/-/tree/main/9%20Supplementary%20notes)
All methods related to supplementary notes 1 and 2 are in this directory and are explained in the README file in this directory.







