#!/bin/bash
#PBS -l nodes=1:ppn=24
#PBS -l walltime=72:00:00
#PBS -q bigmem
#PBS -o /nlustre/users/anneri/HiSeq/fastqc/fastqc_stdout.log
#PBS -e /nlustre/users/anneri/HiSeq/fastqc/fastqc_stdout.log
#PBS -k oe
#PBS -m abe
#PBS -M anneri.lotter@fabi.up.ac.za


export JAVA_OPTS="-Xmx2g"
module load fastqc-0.11.7
fastqc -t 3 -d /nlustre/users/anneri/HiSeq/fastqc -o /nlustre/users/anneri/HiSeq/fastqc/F1_FK118_1 -f fastq /nlustre/users/anneri/HiSeq/F1_FK118_1.fastq.gz

fastqc -t 3 -d /nlustre/users/anneri/HiSeq/fastqc -o /nlustre/users/anneri/HiSeq/fastqc/F1_FK118_2 -f fastq /nlustre/users/anneri/HiSeq/F1_FK118_2.fastq.gz

fastqc -t 3 -d /nlustre/users/anneri/HiSeq/fastqc -o /nlustre/users/anneri/HiSeq/fastqc/URO_FK1556_1 -f fastq /nlustre/users/anneri/HiSeq/URO_FK1556_1.fastq.gz

fastqc -t 3 -d /nlustre/users/anneri/HiSeq/fastqc -o /nlustre/users/anneri/HiSeq/fastqc/URO_FK1556_2 -f fastq /nlustre/users/anneri/HiSeq/URO_FK1556_2.fastq.gz

fastqc -t 3 -d /nlustre/users/anneri/HiSeq/fastqc -o /nlustre/users/anneri/HiSeq/fastqc/URO_FK1755_1 -f fastq /nlustre/users/anneri/HiSeq/URO_FK1755_1.fastq.gz

fastqc -t 3 -d /nlustre/users/anneri/HiSeq/fastqc -o /nlustre/users/anneri/HiSeq/fastqc/URO_FK1755_2 -f fastq /nlustre/users/anneri/HiSeq/URO_FK1755_2.fastq.gz

fastqc -t 3 -d /nlustre/users/anneri/HiSeq/fastqc -o /nlustre/users/anneri/HiSeq/fastqc/GRA_FK1758_1 -f fastq /nlustre/users/anneri/HiSeq/GRA_FK1758_1.fastq.gz

fastqc -t 3 -d /nlustre/users/anneri/HiSeq/fastqc -o /nlustre/users/anneri/HiSeq/fastqc/GRA_FK1758_2 -f fastq /nlustre/users/anneri/HiSeq/GRA_FK1758_2.fastq.gz
