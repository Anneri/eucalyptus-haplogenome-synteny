#!/bin/bash
#PBS -l nodes=1:ppn=28
#PBS -l walltime=08:00:00
#PBS -q normal
#PBS -o /nlustre/users/anneri/EucGUF1_MinIONruns/GuppyCalled_GPU/Run01_GPU/Porechopped_stdout.log
#PBS -e /nlustre/users/anneri/EucGUF1_MinIONruns/GuppyCalled_GPU/Run01_GPU/Porechopped_stderr.log
#PBS -k oe
#PBS -m abe
#PBS -M anneri.lotter@fabi.up.ac.za

module load porechop/0.2.4
porechop -i /nlustre/users/anneri/EucGUF1_MinIONruns/GuppyCalled_GPU/Run01_GPU/GUF1_passReads_MinION_run1.fastq -o /nlustre/users/anneri/EucGUF1_MinIONruns/GuppyCalled_GPU/Run01_GPU/GUF1_passReads_porechopped_MinION_run1.fastq
porechop -i /nlustre/users/anneri/EucGUF1_MinIONruns/GuppyCalled_GPU/MinION_Run02_GPU/GUF1_MinION_Run2.fastq -o /nlustre/users/anneri/EucGUF1_MinIONruns/GuppyCalled_GPU/MinION_Run02_GPU/GUF1_passReads_porechopped_MinION_run2.fastq
porechop -i /projects/EBP/CBC/eucalyptus/rawReads/Nanopore/promethion_sequencing_run1_eucalyptus_F1hybrid.fastq -o /projects/EBP/CBC/eucalyptus/rawReads/Nanopore/GUF1_passReads_porechopped_PromethION_run1.fastq
porechop -i /projects/EBP/CBC/eucalyptus/rawReads/Nanopore/promethion_sequencing_run2_eucalyptus_F1hybrid.fastq -o /projects/EBP/CBC/eucalyptus/rawReads/Nanopore/GUF1_passReads_porechopped_PromethION_run2.fastq






